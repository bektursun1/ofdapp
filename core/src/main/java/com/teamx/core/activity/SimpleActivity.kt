package com.teamx.core.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding

abstract class SimpleActivity<VM: ViewBinding> : AppCompatActivity() {

    protected abstract val vb: VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentViewBinding()
        parseDataFromIntent()
        setupViews()
    }

    open fun setupViews() {}

    open fun parseDataFromIntent() {}

    abstract fun setContentViewBinding()
}
