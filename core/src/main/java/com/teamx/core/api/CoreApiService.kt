package com.teamx.core.api

import com.teamx.core.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit


abstract class CoreApiService<T> {

    private val loggingLevel get() =
        if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
        else HttpLoggingInterceptor.Level.NONE

    protected abstract val loggingInterceptor: HttpLoggingInterceptor

    protected abstract val client: OkHttpClient

    protected abstract val converterFactory: Converter.Factory

    protected abstract val retrofit: Retrofit

    open fun create(service: Class<T>): T {
        return retrofit.create(service)
    }

}