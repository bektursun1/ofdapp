package com.teamx.core.view_model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.teamx.core.Event
import kotlinx.coroutines.launch

abstract class CoreViewModel : ViewModel() {

    private val _event: MutableLiveData<Event> = MutableLiveData()
    val event: LiveData<Event> get() = _event

    fun safeApiCall(func: suspend () -> Unit) {
        viewModelScope.launch {

        }
    }

}
