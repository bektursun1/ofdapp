package com.teamx.utils

import android.content.Context
import android.widget.Toast
import androidx.annotation.StringRes

fun Context.toast(message: String) = Toast.makeText(this, message, Toast.LENGTH_LONG).show()

fun Context.toast(@StringRes textId: Int, duration: Int = Toast.LENGTH_LONG) =
        this.let {
            Toast.makeText(it, textId, duration).show()
        }