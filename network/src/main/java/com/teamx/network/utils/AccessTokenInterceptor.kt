package com.teamx.network.utils

import okhttp3.Interceptor
import okhttp3.Response

class AccessTokenInterceptor(private val accessToken: String) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        request = request.newBuilder().header("Authorization", accessToken).build()
        return chain.proceed(request)
        // header -> session_uuid: d34a4f45-a424-418a-8efe-edc0c108cdf1
        // /pkkm/api/get-secure-key?invalidate_existing=false
    }
}
