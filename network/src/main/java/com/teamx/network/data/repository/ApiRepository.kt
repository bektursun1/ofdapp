package com.teamx.network.data.repository

import com.teamx.data.model.AccessToken
import com.teamx.network.data.web_api.Authorization
import com.teamx.network.data.web_api.AuthorizationApiService
import com.teamx.network.utils.Result
import com.teamx.network.utils.safeApiCall
import com.teamx.storage.Preferences
import kotlinx.coroutines.Dispatchers

class ApiRepository(private val authorization: Authorization, private val prefs: Preferences) {

    suspend fun getAccessToken(username: String, password: String, gnsKey: String): Result<AccessToken> {
        val call = safeApiCall(Dispatchers.IO) {
            authorization.create(AuthorizationApiService::class.java).getAccessToken(username, password, gnsKey = gnsKey)
        }

        if (call is Result.Success) {
            prefs.setAccessToken(call.value.access_token)
        }

        return call
    }

}