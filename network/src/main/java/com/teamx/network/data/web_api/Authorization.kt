package com.teamx.network.data.web_api

import com.google.gson.GsonBuilder
import com.teamx.core.api.CoreApiService
import com.teamx.network.BuildConfig
import com.teamx.network.utils.ApiProperties
import com.teamx.network.utils.AccessTokenInterceptor
import com.teamx.network.utils.UnsafeOkHttpClient
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class Authorization : CoreApiService<AuthorizationApiService>() {

    override val loggingInterceptor: HttpLoggingInterceptor
        get() =  HttpLoggingInterceptor()

    override val client: OkHttpClient
        get() = UnsafeOkHttpClient.getUnsafeOkHttpClient()
            .addInterceptor(AccessTokenInterceptor(ApiProperties.AUTH_ACCESS_TOKEN))
            .addInterceptor(loggingInterceptor.setLevel(if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
            else HttpLoggingInterceptor.Level.NONE))
            .build()

    private val gson = GsonBuilder()
        .setLenient()
        .create()

    override val converterFactory: Converter.Factory
        get() = GsonConverterFactory.create(gson)

    override val retrofit: Retrofit
        get() = Retrofit.Builder()
            .baseUrl(ApiProperties.AUTH_BASE_URL)
            .client(client)
            .addConverterFactory(converterFactory)
            .build()

    override fun create(service: Class<AuthorizationApiService>): AuthorizationApiService {
        return super.create(service)
    }
}