package com.teamx.network.data.web_api

import com.teamx.data.model.AccessToken
import retrofit2.http.POST
import retrofit2.http.Query

interface AuthorizationApiService {

    @POST("pkkm/api/oauth/token?grant_type=password")
    suspend fun getAccessToken(@Query("username") username: String,
                               @Query("password") password: String,
                               @Query("scope") scope: String = "read",
                               @Query("gns_key") gnsKey: String): AccessToken

    @POST("pkkm/api/get-secure-key")
    suspend fun getSecureKey(@Query("invalidate_existing") invalidateExisting: Boolean = false)

}