package com.teamx.network.networkKoin

import com.teamx.network.data.repository.ApiRepository
import com.teamx.network.data.web_api.Authorization
import org.koin.dsl.module

val networkKoin = module {
    single { Authorization() }
    single { ApiRepository(get(), get()) }
}