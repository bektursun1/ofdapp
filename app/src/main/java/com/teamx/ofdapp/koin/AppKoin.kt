package com.teamx.ofdapp.koin

import com.teamx.ofdapp.main.MainViewModel
import com.teamx.ofdapp.main.MainViewModelImpl
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appKoin = module {
    viewModel<MainViewModel> { MainViewModelImpl(get()) }
}