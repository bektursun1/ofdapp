package com.teamx.ofdapp.main

import android.os.Bundle
import com.teamx.core.activity.CoreActivity
import com.teamx.ofdapp.R
import com.teamx.ofdapp.databinding.ActivityMainBinding

class MainActivity : CoreActivity<ActivityMainBinding, MainViewModel>(MainViewModel::class) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override val vb: ActivityMainBinding
        get() = ActivityMainBinding.inflate(layoutInflater)

    override fun setContentViewBinding() {
        setContentView(vb.root)
    }

    override fun setupViews() {
        super.setupViews()
        vm.test()
    }
}