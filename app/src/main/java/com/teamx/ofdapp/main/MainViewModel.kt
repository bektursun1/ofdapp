package com.teamx.ofdapp.main

import com.teamx.core.view_model.CoreViewModel
import com.teamx.ofdapp.repository.AppRepository

abstract class MainViewModel : CoreViewModel() {

    abstract fun test()
}

class MainViewModelImpl constructor(private val repository: AppRepository) : MainViewModel() {
    override fun test() {
        testTest()
    }

    private fun testTest() {
        println("Test test")
    }
}
