package com.teamx.ofdapp.application

import android.app.Application
import com.teamx.network.networkKoin.networkKoin
import com.teamx.ofdapp.koin.appKoin
import com.teamx.storage.storageKoin.storageKoin
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class OFDApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@OFDApplication)
            modules(appKoin, networkKoin, storageKoin)
        }
    }
}