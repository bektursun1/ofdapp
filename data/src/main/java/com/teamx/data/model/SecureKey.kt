package com.teamx.data.model

data class SecureKey(
    val result: String,
    val resultCode: String,
    val details: String? = null, // failure description
    val resultDetail: String? = null // failure code
)
