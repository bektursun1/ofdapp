package com.teamx.storage

import android.content.SharedPreferences

interface Preferences {
    val sharedPreference: SharedPreferences

    fun isSignedIn(): Boolean
    fun setSignedIn(value: Boolean)

    fun getAccessToken(): String
    fun setAccessToken(value: String)

}