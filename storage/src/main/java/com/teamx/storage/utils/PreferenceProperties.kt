package com.teamx.storage.utils

object PreferenceProperties {
    const val SHARED_PREFERENCE_FILE_NAME: String = "com.teamx.storage.utils"
    const val SIGNED_IN: String = "signedIn"
    const val ACCESS_TOKEN: String = "accessToken"
}