package com.teamx.storage.storageKoin

import com.teamx.storage.Preferences
import com.teamx.storage.PreferencesImpl
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

val storageKoin = module {
    single<Preferences> { PreferencesImpl(androidApplication()) }
}