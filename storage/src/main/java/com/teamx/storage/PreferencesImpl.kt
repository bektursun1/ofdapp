package com.teamx.storage

import android.content.Context
import android.content.SharedPreferences
import com.teamx.storage.utils.PreferenceProperties.ACCESS_TOKEN
import com.teamx.storage.utils.PreferenceProperties.SHARED_PREFERENCE_FILE_NAME
import com.teamx.storage.utils.PreferenceProperties.SIGNED_IN

class PreferencesImpl(context: Context) : Preferences {

    override val sharedPreference: SharedPreferences =
        context.getSharedPreferences(SHARED_PREFERENCE_FILE_NAME, Context.MODE_PRIVATE)

    override fun isSignedIn(): Boolean = sharedPreference.getBoolean(SIGNED_IN, false)

    override fun setSignedIn(value: Boolean) {
        sharedPreference.edit().apply {
            this.putBoolean(SIGNED_IN, value)
            apply()
        }
    }

    override fun getAccessToken(): String = sharedPreference.getString(ACCESS_TOKEN, "") ?: ""

    override fun setAccessToken(value: String) {
        sharedPreference.edit().apply {
            this.putString(ACCESS_TOKEN, value)
            apply()
        }
    }

}